/*
 * This file is part of the OpenMV project.
 * Copyright (c) 2013/2014 Ibrahim Abdelkader <i.abdalkader@gmail.com>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 *
 * Framebuffer stuff.
 *
 */
#include "mp.h"
#include "imlib.h"
#include "py_assert.h"
#include "omv_boardconfig.h"
#include "framebuffer.h"
#include "fb_alloc.h"
#include "xalloc.h"

#ifdef OMV_FB_MEMORY
extern char _fb_base;
framebuffer_t *fb_framebuffer = (framebuffer_t *) &_fb_base;
char* fb_framebuffer_top = &_fballoc;
#else
framebuffer_t *fb_framebuffer = NULL;
char* fb_framebuffer_top = NULL;
#endif
const size_t fb_length = OMV_FB_SIZE + OMV_FB_ALLOC_SIZE;

#ifdef OMV_JPEG_MEMORY
extern char _jpeg_buf;
jpegbuffer_t *jpeg_fb_framebuffer = (jpegbuffer_t *) &_jpeg_buf;
#else
jpegbuffer_t *jpeg_fb_framebuffer = NULL;
#endif

void fb_buffer_init(void) {
    bool updated = (fb_framebuffer == NULL);
    #ifndef OMV_FB_MEMORY
    #ifdef OMV_FB_FIXED_ADDRESS
    fb_framebuffer = (framebuffer_t *) OMV_FB_FIXED_ADDRESS;
    #else
    if (fb_framebuffer != NULL) {
        // This essentially verifies if the buffer is still valid
        // which it is not after a soft-reset
        framebuffer_t * prev = fb_framebuffer;
        fb_framebuffer = (framebuffer_t *) gc_realloc(fb_framebuffer, fb_length, true);
        if (fb_framebuffer != prev) {
            updated = true;
        }
    } else {
        fb_framebuffer = (framebuffer_t *) xalloc(fb_length);
    }
    #endif
    fb_framebuffer_top = (char*)fb_framebuffer + fb_length;
    #endif

    #ifndef OMV_JPEG_MEMORY
    #ifdef OMV_JPEG_FIXED_ADDRESS
    jpeg_fb_framebuffer = (jpegbuffer_t *) OMV_JPEG_FIXED_ADDRESS;
    #else
    if (fb_framebuffer != NULL) {
        // This essentially verifies if the buffer is still valid
        // which it is not after a soft-reset
        jpeg_fb_framebuffer = (jpegbuffer_t *) gc_realloc(jpeg_fb_framebuffer, OMV_JPEG_BUF_SIZE, true);
    } else {
        jpeg_fb_framebuffer = (jpegbuffer_t *) xalloc(OMV_JPEG_BUF_SIZE);
    }
    #endif
    #endif
    fb_alloc_init0(updated);
}

uint32_t fb_buffer_size(void)
{
    size_t length = 0;
    switch (MAIN_FB()->bpp) {
        case IMAGE_BPP_BINARY: {
            return ((MAIN_FB()->w + UINT32_T_MASK) >> UINT32_T_SHIFT) * MAIN_FB()->h;
        }
        case IMAGE_BPP_GRAYSCALE: {
            return (MAIN_FB()->w * MAIN_FB()->h) * sizeof(uint8_t);
        }
        case IMAGE_BPP_RGB565: {
            return (MAIN_FB()->w * MAIN_FB()->h) * sizeof(uint16_t);
        }
        default: {
            // jpeg or hasn't been set yet
            if(MAIN_FB()->bpp > 0) {
                length = MAIN_FB()->bpp;
            } else {
                length = OMV_FB_SIZE;
            }
            break;
        }
        PY_ASSERT_TRUE_MSG(length <= fb_length, 
                           "reported frame size too large");
    }
    return length;
}

void fb_update_jpeg_buffer(void)
{
    static int overflow_count = 0;

    if ((MAIN_FB()->bpp > 3) && JPEG_FB()->enabled) {
        // Lock FB
        #if MICROPY_PY_THREAD
        if (mp_thread_mutex_lock(&JPEG_FB()->lock, /* timeout */ 1))
        #endif
        {
            if((OMV_JPEG_BUF_SIZE-64) < MAIN_FB()->bpp) {
                // image won't fit. so don't copy.
                JPEG_FB()->w = 0; JPEG_FB()->h = 0; JPEG_FB()->size = 0;
            } else {
                memcpy(JPEG_FB()->pixels, MAIN_FB()->pixels, MAIN_FB()->bpp);
                JPEG_FB()->w = MAIN_FB()->w; JPEG_FB()->h = MAIN_FB()->h; JPEG_FB()->size = MAIN_FB()->bpp;
            }

            #if MICROPY_PY_THREAD
            // Unlock the framebuffer mutex
            mp_thread_mutex_unlock(&JPEG_FB()->lock);
            #endif
        }
    } else if ((MAIN_FB()->bpp >= 0) && JPEG_FB()->enabled) {
        #if MICROPY_PY_THREAD
        // Lock FB
        if (mp_thread_mutex_lock(&JPEG_FB()->lock, /* timeout */ 1)) 
        #endif
        {
            // Set JPEG src and dst images.
            image_t src = {.w=MAIN_FB()->w, .h=MAIN_FB()->h, .bpp=MAIN_FB()->bpp,     .pixels=MAIN_FB()->pixels};
            image_t dst = {.w=MAIN_FB()->w, .h=MAIN_FB()->h, .bpp=(OMV_JPEG_BUF_SIZE-64),  .pixels=JPEG_FB()->pixels};

            // Note: lower quality saves USB bandwidth and results in a faster IDE FPS.
            bool overflow = jpeg_compress(&src, &dst, JPEG_FB()->quality, false);
            if (overflow == true) {
                // JPEG buffer overflowed, reduce JPEG quality for the next frame
                // and skip the current frame. The IDE doesn't receive this frame.
                if (JPEG_FB()->quality > 1) {
                    // Keep this quality for the next n frames
                    overflow_count = 60;
                    JPEG_FB()->quality = IM_MAX(1, (JPEG_FB()->quality/2));
                }
                JPEG_FB()->w = 0; JPEG_FB()->h = 0; JPEG_FB()->size = 0;
            } else {
                if (overflow_count) {
                    overflow_count--;
                }
                // No buffer overflow, increase quality up to max quality based on frame size
                if (overflow_count == 0 && JPEG_FB()->quality
                       < ((fb_buffer_size() > JPEG_QUALITY_THRESH) ? JPEG_QUALITY_LOW:JPEG_QUALITY_HIGH)) {
                    JPEG_FB()->quality++;
                }
                // Set FB from JPEG image
                JPEG_FB()->w = dst.w; JPEG_FB()->h = dst.h; JPEG_FB()->size = dst.bpp;
            }

            #if MICROPY_PY_THREAD
            // Unlock the framebuffer mutex
            mp_thread_mutex_unlock(&JPEG_FB()->lock);
            #endif
        }
    }
}
