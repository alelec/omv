OMV_MOD_DIR := $(USERMOD_DIR)

ifeq ($(wildcard $(OMV_MOD_DIR)/cmsis/*),)
    $(error cmsis missing, please update submodules in omv: $(OMV_MOD_DIR)/cmsis)
endif

CFLAGS_USERMOD += $(addprefix -I$(OMV_MOD_DIR)/, \
    .                         \
    img                       \
    py                        \
)

ifeq ($(PORT),stm32)
CFLAGS_USERMOD += $(addprefix -I$(OMV_MOD_DIR)/, \
    nn                        \
    cmsis/CMSIS/Core/Include  \
    cmsis/CMSIS/DSP/Include   \
    cmsis/CMSIS/NN/Include    \
)
CFLAGS_USERMOD += -D__FPU_PRESENT=1 -DARM_NN_TRUNCATE
endif

# Sources
SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/,   \
    xalloc.c                  \
    fb_alloc.c                \
    umm_malloc.c              \
    ff_wrapper.c              \
    ini.c                     \
    framebuffer.c             \
    array.c                   \
    trace.c                   \
)

ifeq ($(PORT),stm32)   
SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/,   \
    cambus.c                  \
    ov9650.c                  \
    ov2640.c                  \
    ov7725.c                  \
    mt9v034.c                 \
    lepton.c                  \
    sensor.c                  \
    stm32fxxx_hal_msp.c       \
    soft_i2c.c                \
)
endif

SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/img/,   \
    binary.c                  \
    blob.c                    \
    clahe.c                   \
    draw.c                    \
    qrcode.c                  \
    apriltag.c                \
    dmtx.c                    \
    zbar.c                    \
    fsort.c                   \
    qsort.c                   \
    fft.c                     \
    filter.c                  \
    haar.c                    \
    imlib.c                   \
    collections.c             \
    stats.c                   \
    integral.c                \
    integral_mw.c             \
    kmeans.c                  \
    lab_tab.c                 \
    xyz_tab.c                 \
    yuv_tab.c                 \
    rainbow_tab.c             \
    rgb2rgb_tab.c             \
    invariant_tab.c           \
    mathop.c                  \
    pool.c                    \
    point.c                   \
    rectangle.c               \
    bmp.c                     \
    ppm.c                     \
    gif.c                     \
    mjpeg.c                   \
    fast.c                    \
    agast.c                   \
    orb.c                     \
    template.c                \
    phasecorrelation.c        \
    shadow_removal.c          \
    font.c                    \
    jpeg.c                    \
    lbp.c                     \
    eye.c                     \
    hough.c                   \
    line.c                    \
    lsd.c                     \
    sincos_tab.c              \
    edge.c                    \
    hog.c                     \
    selective_search.c        \
   )

ifeq ($(PORT),stm32)
SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/img/,   \
    fmath.c                   \
)

SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/nn/, \
    nn.c                      \
)
endif

SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/py/, \
    py_helper.c               \
    py_omv.c                  \
    py_image.c                \
    py_time.c                 \
    py_gif.c                  \
    py_mjpeg.c                \
)

ifeq ($(PORT),unix)
SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/py/, \
    py_unix_stubs.c \
)
endif

ifeq ($(PORT),stm32)
SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/py/, \
    py_cpufreq.c              \
    py_fir.c                  \
    py_lcd.c                  \
    py_nn.c                   \
    py_sensor.c               \
)

SRC_USERMOD += $(addprefix $(OMV_MOD_DIR)/cmsis/CMSIS/, \
    DSP/Source/CommonTables/arm_common_tables.c \
    DSP/Source/CommonTables/arm_const_structs.c \
    DSP/Source/FastMathFunctions/arm_cos_f32.c \
    DSP/Source/FastMathFunctions/arm_sin_f32.c \
    NN/Source/ActivationFunctions/arm_relu_q7.c \
    NN/Source/ConvolutionFunctions/arm_convolve_HWC_q7_fast.c \
    NN/Source/ConvolutionFunctions/arm_convolve_HWC_q7_RGB.c \
    NN/Source/ConvolutionFunctions/arm_convolve_HWC_q7_basic.c \
    NN/Source/ConvolutionFunctions/arm_nn_mat_mult_kernel_q7_q15.c \
    NN/Source/ConvolutionFunctions/arm_nn_mat_mult_kernel_q7_q15_reordered.c \
    NN/Source/FullyConnectedFunctions/arm_fully_connected_q7_opt.c \
    NN/Source/NNSupportFunctions/arm_q7_to_q15_reordered_no_shift.c \
    NN/Source/NNSupportFunctions/arm_q7_to_q15_no_shift.c \
    NN/Source/PoolingFunctions/arm_pool_q7_HWC.c \
    NN/Source/SoftmaxFunctions/arm_softmax_q7.c \
)
endif


OMV_MOD_DIRNAME := $(notdir $(abspath $(OMV_MOD_DIR)))

ifeq ($(PORT),stm32)
# ARM CMSIS libraries have a design pattern that triggers strict-aliasing 
# warnings, we are not treating these as errors.
$(BUILD)/$(OMV_MOD_DIRNAME)/cmsis/CMSIS/%.o: COPT += -Wno-error=strict-aliasing
# Ensure cmsis uses its own includes before the the subset available in micropython
$(BUILD)/$(OMV_MOD_DIRNAME)/%.o: CC += -I$(OMV_MOD_DIR)/cmsis/CMSIS/Core/Include
endif

$(BUILD)/$(OMV_MOD_DIRNAME)/%.o: COPT += -Wno-unused-function
