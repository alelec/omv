/*
 * This file is part of the OpenMV project.
 * Copyright (c) 2013/2014 Ibrahim Abdelkader <i.abdalkader@gmail.com>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 *
 * MicroPython header.
 *
 */
#ifndef __MP_H__
#define __MP_H__
#include <stdio.h>
#include <string.h>
#include <py/mpconfig.h>
#include <py/misc.h>
#include <py/qstr.h>
#include <py/misc.h>
#include <py/nlr.h>
#include <py/lexer.h>
#include <py/parse.h>
#include <py/obj.h>
#include <py/objtuple.h>
#include <py/runtime.h>
#include <py/stream.h>
#include <py/gc.h>
#ifndef UNIX
#include "pendsv.h"
#include "gccollect.h"
#include "pin.h"
#include "extint.h"
#include "usb.h"
#endif
#include "extmod/vfs.h"
#include "extmod/vfs_fat.h"
#include "lib/mp-readline/readline.h"

#if UNIX

#include <time.h>

static inline uint32_t HAL_GetTick(void) { 
    struct timespec ct;
    clock_gettime(CLOCK_REALTIME, &ct);
    return (ct.tv_sec * 1000) + (ct.tv_nsec / 1000000);
}

#define __WFI() // nop

#define __inline inline
#define __weak   __attribute__((weak))
#define __STATIC_FORCEINLINE                   __attribute__((always_inline)) static inline

#ifdef NORETURN
#undef NORETURN
#define NORETURN
#endif 

#define __disable_irq(_) // nop
#define __enable_irq(_) // nop

#endif

#endif // __MP_H__
