/*
 * This file is part of the OpenMV project.
 * Copyright (c) 2013/2014 Ibrahim Abdelkader <i.abdalkader@gmail.com>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 *
 * Sensor Python module.
 *
 */
#ifndef __PY_SENSOR_H__
#define __PY_SENSOR_H__

#endif // __PY_SENSOR_H__
