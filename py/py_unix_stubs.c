/*
 * This file is part of the OpenMV project.
 * Copyright (c) 2013/2014 Ibrahim Abdelkader <i.abdalkader@gmail.com>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 *
 * module stubs when compiling for unix port.
 *
 */
#include <mp.h>
#include "py_helper.h"

#define MODULE_UNAVAILABLE(mod) \
static const mp_map_elem_t mod ## _globals_dict_table[] = { \
    { MP_OBJ_NEW_QSTR(MP_QSTR___name__),    MP_OBJ_NEW_QSTR(MP_QSTR_ ## mod)  }, \
    { MP_OBJ_NEW_QSTR(MP_QSTR___init__),    (mp_obj_t)&py_func_unavailable_obj    }, \
    { NULL, NULL }, \
}; \
STATIC MP_DEFINE_CONST_DICT(mod ## _globals_dict, mod ## _globals_dict_table); \
const mp_obj_module_t mod ## _module = { \
    .base = { &mp_type_module }, \
    .globals = (mp_obj_t)&mod ## _globals_dict, \
}; 

MODULE_UNAVAILABLE(cpufreq);
MODULE_UNAVAILABLE(fir);
MODULE_UNAVAILABLE(nn);
MODULE_UNAVAILABLE(lcd);
MODULE_UNAVAILABLE(sensor);

// const mp_obj_module_t nn_module = {
//     .base = { &mp_type_module },
//     .globals = (mp_obj_t)&globals_dict,
// };

// const mp_obj_module_t lcd_module = {
//     .base = { &mp_type_module },
//     .globals = (mp_obj_t)&globals_dict,
// };

// const mp_obj_module_t fir_module = {
//     .base = { &mp_type_module },
//     .globals = (mp_obj_t)&globals_dict,
// };

// const mp_obj_module_t fir_module = {
//     .base = { &mp_type_module },
//     .globals = (mp_obj_t)&globals_dict,
// };

// const mp_obj_module_t fir_module = {
//     .base = { &mp_type_module },
//     .globals = (mp_obj_t)&globals_dict,
// };

// const mp_obj_module_t fir_module = {
//     .base = { &mp_type_module },
//     .globals = (mp_obj_t)&globals_dict,
// };