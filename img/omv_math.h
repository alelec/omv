/* This file is part of the OpenMV project.
 * Copyright (c) 2013-2017 Ibrahim Abdelkader <iabdalkader@openmv.io> & Kwabena W. Agyeman <kwagyeman@openmv.io>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 */
#ifndef __OMV_MATH_H__
#define __OMV_MATH_H__

#include "mp.h"
#include <math.h>

#ifndef UNIX
#include <arm_math.h>
#include "fmath.h"

#define sqrt(x) fast_sqrtf(x)
#define floor(x) fast_floorf(x)
#define ceil(x) fast_ceilf(x)
#define round(x) fast_roundf(x)
#define atan(x) fast_atanf(x)
#define atan2(y, x) fast_atan2f((y), (x))
#define exp(x) fast_expf(x)
#define cbrt(x) fast_cbrtf(x)
#define fabs(x) fast_fabsf(x)
#define log(x) MICROPY_FLOAT_C_FUN(log)(x)
#undef log2
#define log2(x) MICROPY_FLOAT_C_FUN(log2)(x)
#define sin(x) arm_sin_f32(x)
#define cos(x) arm_cos_f32(x)

#else // UNIX

#define round(x) (int)round(x)

#ifndef PI
  #define PI               3.14159265358979f
#endif

/**
  \brief   Reverse bit order of value
  \details Reverses the bit order of the given value.
           From cmsis/CMSIS/Core_A/Include/cmsis_gcc.h
  \param [in]    value  Value to reverse
  \return               Reversed value
 */
__STATIC_FORCEINLINE  uint32_t __RBIT(uint32_t value)
{
  uint32_t result;

  int32_t s = (4U /*sizeof(v)*/ * 8U) - 1U; /* extra shift needed at end */

  result = value;                      /* r will be reversed bits of v; first get LSB of v */
  for (value >>= 1U; value; value >>= 1U)
  {
    result <<= 1U;
    result |= value & 1U;
    s--;
  }
  result <<= s;                        /* shift when v's highest bits are zero */
  return result;
}
/**
  \brief   dual_multiply_accumulate
  \details http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0491c/CJADDFJJ.html
 */
__STATIC_FORCEINLINE  uint32_t __SMLAD(uint32_t val1, uint32_t val2, uint32_t val3)
{
    uint32_t p1, p2;
    p1 = (val1 & 0xFFFF) * (val2 & 0xFFFF);
    p2 = ((val1>>16) & 0xFFFF) * ((val2>>16) & 0xFFFF);
    return p1 + p2 + val3;
}

/**
  \brief   Count leading zeros
           From cmsis/CMSIS/Core_A/Include/cmsis_gcc.h
  \param [in]  value  Value to count the leading zeros
  \return             number of leading zeros in value
 */
#define __CLZ                             (uint8_t)__builtin_clz

#define __PKHBT(ARG1,ARG2,ARG3)          ( ((((uint32_t)(ARG1))          ) & 0x0000FFFFUL) |  \
                                           ((((uint32_t)(ARG2)) << (ARG3)) & 0xFFFF0000UL)  )

#define __PKHTB(ARG1,ARG2,ARG3)          ( ((((uint32_t)(ARG1))          ) & 0xFFFF0000UL) |  \
                                           ((((uint32_t)(ARG2)) >> (ARG3)) & 0x0000FFFFUL)  )

/**
  \brief   Reverse byte order (32 bit)
  \details Reverses the byte order in unsigned integer value. For example, 0x12345678 becomes 0x78563412.
  \param [in]    value  Value to reverse
  \return               Reversed value
 */
#define __REV(value)   __builtin_bswap32(value)

/**
  \brief   Rotate Right in unsigned value (32 bit)
  \details Rotate Right (immediate) provides the value of the contents of a register rotated by a variable number of bits.
  \param [in]    op1  Value to rotate
  \param [in]    op2  Number of Bits to rotate
  \return               Rotated value
 */
__STATIC_FORCEINLINE uint32_t __ROR(uint32_t op1, uint32_t op2)
{
  op2 %= 32U;
  if (op2 == 0U)
  {
    return op1;
  }
  return (op1 >> op2) | (op1 << (32U - op2));
}

/**
  \brief   Reverse byte order (16 bit)
  \details Reverses the byte order within each halfword of a word. For example, 0x12345678 becomes 0x34127856.
  \param [in]    value  Value to reverse
  \return               Reversed value
 */
#define __REV16(value) __ROR(__REV(value), 16)

#endif

extern const float cos_table[360];
extern const float sin_table[360];

#endif //__OMV_MATH_H__